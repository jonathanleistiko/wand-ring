// FastLED demo reel code&animations by Mark Kriegsman, December 2014
// Accelerometer code and libraries by Adafruit.

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_LIS3DH.h>
#include <Adafruit_Sensor.h>

// Used for software SPI
#define LIS3DH_CLK 13
#define LIS3DH_MISO 12
#define LIS3DH_MOSI 11
// Used for hardware & software SPI
#define LIS3DH_CS 10

// I2C
Adafruit_LIS3DH lis = Adafruit_LIS3DH();

int xshift=0;
int yshift=0;
int zshift=0;
int xVal=128;
int yVal=128;
int zVal=128;

/////////////////////// START LED stuff
#include "FastLED.h"

FASTLED_USING_NAMESPACE

#define DATA_PIN    14
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    30
CRGB leds[NUM_LEDS];

#define BRIGHTNESS  60
#define FRAMES_PER_SECOND  60

int hue=0;

#define BRIGHTNESS  85
#define FRAMES_PER_SECOND 60

CRGBPalette16 gPal;

// -- The core to run FastLED.show()
#define FASTLED_SHOW_CORE 0

// -- Task handles for use in the notifications
static TaskHandle_t FastLEDshowTaskHandle = 0;
static TaskHandle_t userTaskHandle = 0;

//////// GAME VARIABLES ////////
int cursorLocation = 70;
int cursorColor = random (0, 255);
int interpolatedCursorLocation = 7;

int targetExpiration = 0;
int targetLocation = 15;

int coherenceCounter = 0;
int wandColor = 100;
int victoryPoints = 0;

//////// END GAME VARIABLES ////////

void FastLEDshowESP32()
{
    if (userTaskHandle == 0) {
        // -- Store the handle of the current task, so that the show task can
        //    notify it when it's done
        userTaskHandle = xTaskGetCurrentTaskHandle();

        // -- Trigger the show task
        xTaskNotifyGive(FastLEDshowTaskHandle);

        // -- Wait to be notified that it's done
        const TickType_t xMaxBlockTime = pdMS_TO_TICKS( 200 );
        ulTaskNotifyTake(pdTRUE, xMaxBlockTime);
        userTaskHandle = 0;
    }
}

/** show Task
 *  This function runs on core 0 and just waits for requests to call FastLED.show()
 */
void FastLEDshowTask(void *pvParameters)
{
    // -- Run forever...
    for(;;) {
        // -- Wait for the trigger
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        // -- Do the show (synchronously)
        FastLED.show();

        // -- Notify the calling task
        xTaskNotifyGive(userTaskHandle);
    }
}

////////// END LED stuff

void setup(void) {
  delay(3000); // 3 second delay for recovery
  Serial.begin(115200);

////////// START LED Setup
  // Turn on power to the PropMaker FeatherWing LED JST strip connection
  pinMode(33, OUTPUT);
  digitalWrite(33, HIGH);
  Serial.print ("PropMaker powered up!");

  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  // set brightness control
  FastLED.setBrightness(BRIGHTNESS);

  int core = xPortGetCoreID();
  Serial.print("Main code running on core ");
  Serial.println(core);

  // -- Create the FastLED show task
  xTaskCreatePinnedToCore(FastLEDshowTask, "FastLEDshowTask", 2048, NULL, 2, &FastLEDshowTaskHandle, FASTLED_SHOW_CORE);

///////// END LED SETUP
  
///////// START ACCELEROMETER SETUP
  Serial.println("LIS3DH test:");
  
  if (! lis.begin(0x18)) {   // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1);
  }
  Serial.println("LIS3DH found!");
  
  lis.setRange(LIS3DH_RANGE_4_G);   // 2, 4, 8 or 16 G!
  
  Serial.print("Range = "); Serial.print(2 << lis.getRange());  
  Serial.println("G");
///////// END ACCELEROMETER SETUP
}

///////// MAIN LOOP
void loop() {
  readTheWand();
  updateCursor();
  wandColorCycle();
  updateCursorDisplay();
  createTarget(); // Creates and displays
  checkForCoherence();
}
///////// END MAIN LOOP

void readTheWand() {
  lis.read();      // get X Y and Z data at once
  // Then print out the raw data
  // Ranges from -8000 to +8000. Want to nromalize it a bit. 
  xshift=(lis.x);
  xVal=(xshift+8000)/60;
  yshift=(lis.y);
  yVal=(yshift+8000)/60;
  zshift=(lis.z);
  zVal=(zshift+8000)/60;

  if (xVal<0) {xVal=0;} else if (xVal>255) {xVal=255;}
  if (yVal<0) {yVal=0;} else if (yVal>255) {yVal=255;}
  if (zVal<0) {zVal=0;} else if (zVal>255) {zVal=255;}
  
  Serial.print("xVal: "); Serial.print(xVal); 
  Serial.print("\tyVal: "); Serial.print(yVal); 
  Serial.print("\tzVal: "); Serial.print(zVal); 
  Serial.println();
}

void updateCursor() {
  eraseOldCursor();
  if (yVal < 115) {
    cursorLocation--;
    if (yVal < 105) {
      cursorLocation--;
      if (yVal < 85) {
        cursorLocation--;
        if (yVal < 45) {
          cursorLocation--;
        }
      }
    }
  }
  if (cursorLocation < 0) {cursorLocation=0;}
  if (yVal > 135) {
    cursorLocation++;
    if (yVal > 145) {
      cursorLocation++;
      if (yVal > 165) {
        cursorLocation++;
        if (yVal > 205) {
          cursorLocation++;
        }
      }
    }
  }
  if (cursorLocation > 140) {cursorLocation=140;}
}

void eraseOldCursor() {
  interpolatedCursorLocation = cursorLocation/10;
  leds[interpolatedCursorLocation] = CRGB::Black;
}

void updateCursorDisplay() {
  interpolatedCursorLocation = cursorLocation/10;
  leds[interpolatedCursorLocation] = CHSV(cursorColor, 50, 200);
  FastLEDshowESP32();
  FastLED.delay(1000/FRAMES_PER_SECOND); // insert a delay to keep the framerate modest
}

void createTarget() {
  if (millis() >= targetExpiration) {
    leds[targetLocation] = CRGB::Black;
    targetLocation = targetLocation + random(3,10);
    if (targetLocation > 29) {targetLocation-=15;}
    targetExpiration = millis() + random (2000, 4000);
  }
  leds[targetLocation] = CHSV (random(0,255),200,200);
  FastLEDshowESP32();
}

void checkForCoherence() {
  if ((29-targetLocation)==interpolatedCursorLocation) {
    coherenceCounter +=1;
      if (coherenceCounter >= 30) {
        victoryPoints += 1;
        if (victoryPoints >= 255) {cursorColor = random(0,255);}
      }
  } else {
    coherenceCounter -= 1;
    if (coherenceCounter<0) {coherenceCounter=0;}
  }
}

void wandColorCycle() {
  wandColor += 1;
  if (wandColor >255) {wandColor = 0;}
  fill_solid(leds, NUM_LEDS, CHSV(wandColor,victoryPoints,victoryPoints)); // Color the wand  
}

void oldLoop() {
    lis.read();      // get X Y and Z data at once
  // Then print out the raw data
  // Ranges from -8000 to +8000. Want to nromalize it a bit. 
  xshift=(lis.x);
  xVal=(xshift+8000)/60;
  yshift=(lis.y);
  yVal=(yshift+8000)/60;
  zshift=(lis.z);
  zVal=(zshift+8000)/60;

  if (xVal<0) {xVal=0;} else if (xVal>255) {xVal=255;}
  if (yVal<0) {yVal=0;} else if (yVal>255) {yVal=255;}
  if (zVal<0) {zVal=0;} else if (zVal>255) {zVal=255;}
  
  Serial.print("xVal: "); Serial.print(xVal); 
  Serial.print("\tyVal: "); Serial.print(yVal); 
  Serial.print("\tzVal: "); Serial.print(zVal); 
  Serial.println();

  fill_gradient(leds,2,CHSV(xVal,255,255),15,CHSV(xVal,255,0),SHORTEST_HUES); 
  fill_gradient(leds,29,CHSV(yVal,255,255),16,CHSV(yVal,255,0),SHORTEST_HUES);
 
  
  FastLEDshowESP32();
  FastLED.delay(1000/FRAMES_PER_SECOND); // insert a delay to keep the framerate modest
 
//  delay(200); 
}
