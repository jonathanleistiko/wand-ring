#include <FastLED.h>

// Accelerometer libraries and such...
#include <Wire.h>
#include <SPI.h> // Needed if using the PropMaker Wing accelerometer
#include <Adafruit_LIS3DH.h> // For the PropMaker accelerometer
#include <Adafruit_Sensor.h> // For the PropMaker accelerometer

// Used for software SPI
#define LIS3DH_CLK 13
#define LIS3DH_MISO 12
#define LIS3DH_MOSI 11
// Used for hardware & software SPI
#define LIS3DH_CS 10
// software SPI
Adafruit_LIS3DH lis = Adafruit_LIS3DH(); // Instantiate access to the Accelerometer

// LED Definitions
#define NUM_LEDS 30
#define DATA_PIN 14

// Define the array of leds 
CRGB leds[NUM_LEDS];

// Variables for accelerometer data simplification
float xreal = 0;
float yreal = 0;
float zreal = 0;
float xprime = 0; // Smoothed and simplified
float yprime = 0; // Smoothed and simplified
float zprime = 0; // Smoothed and simplified
float primesum = xprime+yprime+zprime; // Sum of all three simplified accels

// The wand can be in one of 8 positions, associated with the numbers 0 to 7.
String positionQueue = "88888888888888888888"; // The wand remembers the last 20 actions.
// Note: Seeding the positionQueue string with more (or fewer) characters changes how many positions the wand remembers.
String currentPosition = "8"; // The wand knows what its current positon is.
String recentPosition = "8"; // The wand knows what its most recent prior position was.

// Audio
int octave = 5; // Set the octave for notes / audio

////////////////////////////////////////////////////////////
////////////////////// Game Variables //////////////////////
////////////////////////////////////////////////////////////

CRGB alienAttackerColor; // Stores alien attacker info.
CRGB energyBallColor; // Stores energy ball color.
CRGB nextEnergyBallPosition; // Stores energy ball color.
int attackTimer = 0;
int numberOfAliens = 0;
int numberOfTurnsElapsed = 0;
int numberOfWins = 0;
int aliensBanished = 0;

////////////////////////////////////////////////////////////
/////////////////////END Game Variables/////////////////////
////////////////////////////////////////////////////////////

///////////////////// SETUP /////////////////////
void setup() { 
  randomSeed(analogRead(0));
  Serial.begin (115200);

///////////////////////// LED Setup
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  pinMode(33, OUTPUT); // Turn on power to the PropMaker FeatherWing LED JST strip and speaker
  digitalWrite(33, HIGH); // Turn on power to the PropMaker FeatherWing LED JST strip and speaker
  Serial.print ("PropMaker powered up!");

///////////////////////// Accelerometer Setup
  if (! lis.begin(0x18)) {   // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1);
  }
  Serial.println("LIS3DH found!");
  lis.setRange(LIS3DH_RANGE_8_G);   // 2, 4, 8 or 16 G
  Serial.print("Range = "); Serial.print(2 << lis.getRange());  
  Serial.println("G");
  #define CLICKTHRESHHOLD 80 // Adjust this number for the sensitivity of the 'click' force
  lis.setClick(1, CLICKTHRESHHOLD); // 0 = turn off click detection & interrupt • 1 = single click only interrupt output • 2 = double click only interrupt output, detect single click
  ///////////////////////// End Accelerometer Setup

///// Audio Setup ////// NOTE: If you comment out the next two lines, the game will not make noise.
  ledcSetup(0,1E5,12);
  ledcAttachPin(26,0); // Send audio out via pin 26. This is un-changable.
  ///// Audio Setup End /////
}
///////////////////// END SETUP /////////////////////

///////////////////////////////////////////////////////////////
///////////////////////Main Game Loop//////////////////////////
///////////////////////////////////////////////////////////////
void loop() { 
  getWandOrientation();
  updateAliens();
}
//////////////////////////////////////////////////////////////
///////////////////////END Main Loop//////////////////////////
//////////////////////////////////////////////////////////////

// Get the real accel of the want and smooth / simplfy it.
void smoothData () {
  /* Create an event structure to read the accelerometer with */
  sensors_event_t event; 
  lis.getEvent(&event);
  
  /* Set "real" accel variables equal to readings, refactored to one gee */
  xreal = event.acceleration.x/9.8;
  yreal = event.acceleration.y/9.8;
  zreal = event.acceleration.z/9.8;

  /* "Round" real gee readings to nearest .5 (with a reasonable noise buffer and "stickyness")
  and stash the rounded gee in the corresponding "prime" variable. */
  if ((xreal >= -1) && (xreal <= -.85)) {
    xprime =-1;
  } else if ((xreal >= -.65) && (xreal <= -.35)) {
    xprime =-.5;
  } else if ((xreal >= -.15) && (xreal <= .15)) {
    xprime =0;
  } else if ((xreal >= .35) && (xreal <= .65)) {
    xprime =.5;
  } else if ((xreal >= .85) && (xreal <= 1)) {
    xprime =1;
  }
   
  if ((yreal >= -1) && (yreal <= -.85)) {
    yprime =-1;
  } else if ((yreal >= -.65) && (yreal <= -.35)) {
    yprime =-.5;
  } else if ((yreal >= -.15) && (yreal <= .15)) {
    yprime =0;
  } else if ((yreal >= .35) && (yreal <= .65)) {
    yprime =.5;
  } else if ((yreal >= .85) && (yreal <= 1)) {
    yprime =1;
  }
   
  if ((zreal >= -1) && (zreal <= -.85)) {
    zprime =-1;
  } else if ((zreal >= -.65) && (zreal <= -.35)) {
    zprime =-.5;
  } else if ((zreal >= -.15) && (zreal <= .15)) {
    zprime =0;
  } else if ((zreal >= .35) && (zreal <= .65)) {
    zprime =.5;
  } else if ((zreal >= .85) && (zreal <= 1)) {
    zprime =1;
  } 
}

// Turn smoothed accel data into a  simplified position character:
// U = Up, D = Down, L = Left, R = Right, A = straight Ahead, B = pointing back
// u = ahead and "u"p, at a 45º angle, d = ahead and  "d"own at a 45º angle
// These eight positions are all the wand really needs to know in order to cast a spell
// Having more positions than this makes it easier to mess a spell up.
void translatePosition () {
  primesum = xprime+yprime+zprime;

  if (primesum == 1 || primesum == -1) {
    if (xprime == 1) {
      currentPosition = "A"; // Ahead (Was Right)
    } else if (xprime == -1) {
      currentPosition = "B"; // Back (or inverted) (Was Left)
    } else if (yprime == 1) {
      currentPosition = "D"; // Down --- ok
    } else if (yprime == -1) {
      currentPosition = "U"; // Up --- ok
    } else if (zprime == 1) {
      currentPosition = "L"; // Left (Was Ahead)
    } else if (zprime == -1) {
      currentPosition = "R"; // Right (Was Back)
    } else if (yprime == .5) {
      if (xprime == .5) { //was zprime
        currentPosition = "d"; // Ahead down @ 45º up ... ok
      }
    } else if (yprime == -.5) {
      if (xprime == -.5) {
        currentPosition = "B"; // Back, technically at 45º, but we're clustering it into the "back" category ... ok
      }
    }
  }

  if (primesum == 0 ) {
    if (yprime == -.5) { 
        if (xprime == .5) { // Was zprime
          currentPosition = "u"; // Ahead, up @ 45º ... ok
        }
    }
  }
}// END translatePosition

//Record the wand's orientation
void getWandOrientation() {
  smoothData ();
  translatePosition();
  if (currentPosition != recentPosition) // Did the position change? If so...
  {
    recentPosition = currentPosition; // Update the old position to match the current position.
    playPositionTone(); // Make a little beep!
    positionQueue += currentPosition; // Add the new position to the Position Queue.
    positionQueue.remove(0,1);  // Serial.println("New Position Queue:");  Serial.println(positionQueue); // Remove the oldest character from the left of the Position Queue string.
    setEnergyBallColor();
    checkForThrow();
  }
}

void playPositionTone() {
  int toneDuration = 100;
  if (currentPosition=="A") {
    ledcWriteNote(0,NOTE_C,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=="d") {
    ledcWriteNote(0,NOTE_D,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=="u") {
    ledcWriteNote(0,NOTE_E,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=="L") {
    ledcWriteNote(0,NOTE_F,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=="U") {
    ledcWriteNote(0,NOTE_G,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=="D") {
    ledcWriteNote(0,NOTE_A,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=="R") {
    ledcWriteNote(0,NOTE_B,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=="B") {
    ledcWriteNote(0,NOTE_C,octave+1); delay(toneDuration);
    ledcWriteTone(0,0);
  }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Gun Update Stuff //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void setEnergyBallColor() {
  // select a color based on the direction varaible and fill the handle with it
//  if (currentPosition == "d") {
//    energyBallColor = (CRGB::Green);
//  } else if (currentPosition == "D") {
//    energyBallColor = (CRGB::IndianRed);
//  } else if (currentPosition == "A") {
//    energyBallColor = (CRGB::White);
//  } else if (currentPosition == "U") {
//    energyBallColor = (CRGB::Cyan);
//  } else if (currentPosition == "u") {
//    energyBallColor = (CRGB::Yellow);
//  } else if (currentPosition == "B") {
//    energyBallColor = (CRGB::Magenta);
/*  } else*/ if (currentPosition == "L") {
    energyBallColor = (CRGB::Red);
  } else if (currentPosition == "R") {
    energyBallColor = (CRGB::Blue);
  }
  displayEnergyBall();
}

void displayEnergyBall() {
  // We have our color, let's display it!
  leds[0] = energyBallColor;
  leds[NUM_LEDS-1] = energyBallColor;
  FastLED.show();
}

void checkForThrow() {
  // If the wand is pointing up or down, throw the ball.
  if ((currentPosition == "d") || (currentPosition == "D") || (currentPosition == "U") || (currentPosition == "u")) {
    throwTheBall();
  }
}

void throwTheBall () {
  int collision = 0;
  int energyBallPosition = 0;
  while (collision==0) {
    nextEnergyBallPosition = leds[energyBallPosition+1]; // Peek ahead one step in the strip.
    if (!nextEnergyBallPosition) { // If the next space is vacant... Note - ( leds[i] ) is false if vacant and true if it has any non-Black value.
      leds[energyBallPosition] = CRGB::Black; // Black out the current energy ball postion,
      leds[29-energyBallPosition] = CRGB::Black; // Black out the current energy ball postion,
      energyBallPosition ++; // move a step ahead
      leds[energyBallPosition] = energyBallColor; // Draw the e-ball in its new location
      leds[29-energyBallPosition] = energyBallColor; // Draw the e-ball in its new location
      FastLED.show(); // and update the display
      delay(20); // Slow the ball down just a little...
    } else {
      collision = 1; // the energy ball has hit an alien!
      alienHit();
    }
  }
}

void alienHit() {
  Serial.println("Alien hit!");
  CRGB alienColor = leds[15+numberOfAliens-1]; // look at the first alien in line
  int hitAlienRed = leds[15+numberOfAliens-1].red; // what is the red value of the first alien in line?
  int energyBallRed = energyBallColor.red; // what is the red value of the energy ball?
  int hitAlienBlue = leds[15+numberOfAliens-1].blue; // what is the blue value of the first alien in line?
  int energyBallBlue = energyBallColor.blue; // what is the blue value of the energy ball?
  if (energyBallColor == alienColor) { // If the alien and the energy ball match colors
    Serial.println("Alien destroyed!");
    leds[15+numberOfAliens-1] = CRGB::Black; // erase the alien on the left
    leds[15+numberOfAliens] = CRGB::Black; // erase the bullet on the left
    leds[14-numberOfAliens+1] = CRGB::Black; // erase the alien on the right
    leds[14-numberOfAliens] = CRGB::Black; // erase the bullet on the right
    numberOfAliens --; // reduce the # of aliens - do this after you erase them or your count is off!
    aliensBanished ++; // increase the number of aliens banished.
    FastLED.show();
    if (aliensBanished > 99) {superWinLedFx();loseTheGame();}
    if (numberOfAliens < 1){winTheGame();}
  } else {
    Serial.println("Wrong color!!");
    leds[15+numberOfAliens] = CRGB::Black; // erase the bullet on the left
    leds[14-numberOfAliens] = CRGB::Black; // erase the bullet on the right
    FastLED.show();
    // add one to the mistake counter
  }
}

void winTheGame() {
  Serial.println("You won!");
  numberOfWins ++;
  for (int j=0; j<numberOfWins; j++) {
    for (int i=0; i<256; i++) {
      fill_rainbow( leds, NUM_LEDS, i, 10);
      FastLED.show(); // update the strip with all changes
    }
  }
  FastLED.clear (); // Clear the strip
  FastLED.show(); // update the strip with all changes
  if (aliensBanished > 99) {
    superWinLedFx();
  }
  restartTheGame();
}

void restartTheGame() {
  Serial.println("Resetting game.");
  if (numberOfAliens > 13) {numberOfTurnsElapsed=0;} // Player lost. Reset the turn and win count.
  numberOfAliens = 0;
  for (int headstart=0; headstart<2+numberOfWins; headstart++) { // Set up the board with three aliens.
    addAnAlien(); delay(100);
  }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Alien Update Stuff //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void updateAliens() {
  int timeDecay = numberOfTurnsElapsed*10;
  if (timeDecay > 900) {timeDecay=900;}
  attackTimer ++ ;
  if (attackTimer > 1000-(timeDecay)) { // The game speeds with each passing turn
    attackTimer=0; // Reset the timer
    numberOfTurnsElapsed ++; // Add one to the number of turns elapsed.
    addAnAlien();
  }
}

void addAnAlien() {
  numberOfAliens ++; // Increment the number of aliens, of course!
  if (numberOfAliens > 14) {loseTheGame();} // If there are too many aliens, the base has been overwhelmed. End the game.
  selectARandomAlien();
  displayTheAlien();
      ledcWriteNote(0,NOTE_C,octave-3); delay(100); ledcWriteTone(0,0);
}

void selectARandomAlien() {
  int randomAlien = random(2);
//  Serial.print (randomAlien);
  if (randomAlien < 1) {
    alienAttackerColor = (CRGB::Red);
  } else {
    alienAttackerColor = (CRGB::Blue);
  }
}

void displayTheAlien() {
  // Move all the aliens up one step.
  for (int alienCongaLine=numberOfAliens; alienCongaLine > 0; alienCongaLine--) {
    leds[15+alienCongaLine] = leds[15+alienCongaLine-1]; // Update the left side.
    leds[14-alienCongaLine] = leds[14-alienCongaLine+1]; // Update the right side.
  }
  leds[NUM_LEDS/2] = alienAttackerColor; // Display the alien on the left...
  leds[(NUM_LEDS/2)-1] = alienAttackerColor; // ...and on the right.
  FastLED.show();
}

void loseTheGame() {
  Serial.println("You lost!");
  displayScore();
  endGameFx();
  restartTheGame();
}

void endGameFx() {
  for (int i=0; i<256; i++) {
    fill_solid( leds, NUM_LEDS, CHSV(i,255,255-i));
    FastLED.show(); // update the strip with all changes
    delay(15);
  }
  FastLED.clear (); // Clear the strip
  FastLED.show(); // update the strip with all changes
}

void displayScore() {
  for (int roundScore=0; roundScore<numberOfWins; roundScore++) {
    // display nubmer of wins
  }
  int scorePosition=0;
  FastLED.clear (); // Clear the strip
  for (int alienScore=0; alienScore<aliensBanished; alienScore++) {
    leds[scorePosition]=CHSV(scorePosition*10, 255, 255);
    FastLED.show(); 
    scorePosition++; if (scorePosition>29) {scorePosition=0; fill_solid(leds, NUM_LEDS, CHSV(100,100,100));FastLED.show(); }
    delay(25);
  }
  numberOfWins = 0; aliensBanished = 0; // Game's over. Wipe the scores.
}

void superWinLedFx() {
  Serial.println("SUPER WIN!");
  for (int q=0; q<999; q++) {
    if (random(10)<1) {
      int randomHue=random(256);
      int randomLED=random(30);
      leds[randomLED] = CHSV ((randomHue), 200, 255);
      FastLED.show();
      delay(15);
      blur1d(leds, NUM_LEDS, 60);
    }
  }
}
