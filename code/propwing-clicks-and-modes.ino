/* Wand Ring Prototype Dueling Code
(c) 2018, 2019, Jonathan A. Leistiko
contains:
• libraries and modified code from Adafruit
• FastLED code by "kreigsman" - https://gist.github.com/kriegsman/062e10f7f07ba8518af6 */

///////////////////////// Accelerometer Libraries
#include <Wire.h>
#include <SPI.h> // If using the PropMaker Wing
#include <Adafruit_LIS3DH.h> // If using the PropMaker Wing
// #include <Adafruit_MMA8451.h> // If using the seperate MMA8451
#include <Adafruit_Sensor.h>

// Used for software SPI
#define LIS3DH_CLK 13
#define LIS3DH_MISO 12
#define LIS3DH_MOSI 11
// Used for hardware & software SPI
#define LIS3DH_CS 10
// software SPI
Adafruit_LIS3DH lis = Adafruit_LIS3DH(); // Instantiate access to the Accelerometer

///////////////////////// LED (NeoPixel) Driver Libraries
#include "FastLED.h"

// LED pinout(s)
#define DATA_PIN    14

// Other FASTLED Variables
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    30
CRGB leds[NUM_LEDS];

#define BRIGHTNESS          60
#define FRAMES_PER_SECOND  120

// -- The core to run FastLED.show()
#define FASTLED_SHOW_CORE 0

// -- Task handles for use in the notifications
static TaskHandle_t FastLEDshowTaskHandle = 0;
static TaskHandle_t userTaskHandle = 0;
// END LED Variables

int octave = 5; // Set the octave for notes / audio

///////////////////////// Wand Variables
// The wand button controls what mode the wand is in. (Doodle, casting, etc.)
#define BUTTON 27
int buttonValue = 0;
int oldButtonValue = 0; // The wand knows if the button is pressed, what its prior state was,
int pressCount = 1; // and how many times it's been pressed. NOTE: Should start at 0, but I don't have the button rigged yet and want to check doodle mode. Doodle mode is "3".
int TotalNumberOfClicks = 0; // How many times has the mode trigger been triggered? (For debugging. Not essential for core functionality.)

// Variables for accelerometer data simplification
float xreal = 0;
float yreal = 0;
float zreal = 0;
float xprime = 0; // Smoothed and simplified
float yprime = 0; // Smoothed and simplified
float zprime = 0; // Smoothed and simplified
float primesum = xprime+yprime+zprime; // Sum of all three simplified accels

// The wand can be in one of 8 positions, associated with the numbers 0 to 7.
String positionQueue = "88888888888888888888"; // The wand remembers the last 20 actions.
// Note: Seeding the positionQueue string with more (or fewer) characters changes how many positions the wand remembers.
String currentPosition = "8"; // The wand knows what its current positon is.
String recentPosition = "8"; // The wand knows what its most recent prior position was.

//////////////////////////////////////////////////////
/////////////// Gremlin Game Variables ///////////////
//////////////////////////////////////////////////////

CRGB alienAttackerColor; // Stores alien attacker info.
CRGB energyBallColor; // Stores energy ball color.
CRGB nextEnergyBallPosition; // Stores energy ball color.
int attackTimer = 0;
int numberOfAliens = 0;
int numberOfTurnsElapsed = 0;
int numberOfWins = 0;
int aliensBanished = 0;

//////////////////////////////////////////////////////////
/////////////// END Gremlin Game Variables ///////////////
//////////////////////////////////////////////////////////

///////////////////////// Spell Variables
// Eventually you'll put an array of spells here.
// The spell array should contain the spell name, mana cost,
// utility (attack, defense, other), type (earth, metal, etc.),
// magnitude (damage, defense, heal)

///////////////////////// CommsLibraries
// Will go here when I know what to do.

////////////////////////// Comms Setup
// Will go here when I know what to do

// Wand and Status Variables
int doodleHue = 0; // Hue only used in Doodle mode.
// int listenDuration = 0; //How long to wait before casting a spell - This might be unused...
long currentThreshold = millis(); //Start tracking the time in milliseconds.
int thresholdGapValue = 100; //Delay to gain mana; Default: to 1/10th of a second.
long nextThreshold = currentThreshold+thresholdGapValue; //Next threshold is in 1/10th of a second.
int currentMana = 150; // Start the duel with 150 points of mana.
int manaIncrementValue = 1; //You gain 1 mana every threshold; Default: 10 mana per second.
int maximumMana = 250; // Maximum mana storage; default: 200. Cast well and cast often!
int wandDurability = 1000; //Your wand starts with 1,000 points of durability (HP).
bool amIHexed = false; //You do not start hexed.
String mostRecentHex = "8888888888888"; //What were we hit with most recently?
long whenIWasHexed = 0; //What time was it when I was most recently hexed?
int hexGapValue = 15000; //Time to react to a hex; Default: 15 seconds.
String currentSpell = "8888888888888"; // What spell are you casting right now?
String mostRecentSpell = "8888888888888"; //What spell did we cast most recently?
int repeatedSpellCount = 0; // How many times have you cast the same spell?
int repeatedSpellPenalty = 20; // You lose 20 mana for each time you repeated the spell.
long whenIHexedTheOpponent = 0; // When did you most recently hex your opponent?
bool isOpponentHexed = false; // Your opponent starts off un-hexed.
// End wand and status variables

// FastLED Elemental Jet Variables
CRGBPalette16 gPal;
#define COOLING  30 // COOLING: How much does the air cool as it rises? Less cooling = taller flames.  More cooling = shorter flames.
#define SPARKING 30 // SPARKING: What chance (out of 255) is there that a new spark will be lit? Higher chance = more roaring fire.  Lower chance = more flickery fire.
// END FastLED Elemental Jet variables

// FastLED Functions, Section 1
/** show() for ESP32
 *  Call this function instead of FastLED.show(). It signals core 0 to issue a show, 
 *  then waits for a notification that it is done.
 */
void FastLEDshowESP32()
{
    if (userTaskHandle == 0) {
        // -- Store the handle of the current task, so that the show task can
        //    notify it when it's done
        userTaskHandle = xTaskGetCurrentTaskHandle();

        // -- Trigger the show task
        xTaskNotifyGive(FastLEDshowTaskHandle);

        // -- Wait to be notified that it's done
        const TickType_t xMaxBlockTime = pdMS_TO_TICKS( 200 );
        ulTaskNotifyTake(pdTRUE, xMaxBlockTime);
        userTaskHandle = 0;
    }
}

/** show Task
 *  This function runs on core 0 and just waits for requests to call FastLED.show()
 */
void FastLEDshowTask(void *pvParameters)
{
    // -- Run forever...
    for(;;) {
        // -- Wait for the trigger
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        // -- Do the show (synchronously)
        FastLED.show();

        // -- Notify the calling task
        xTaskNotifyGive(userTaskHandle);
    }
}
// END FastLED Functions, section 1

////////// START SETUP //////////
void setup() 
{
  delay(3000); // 3 second delay for NeoPixel safety - don't want to burn them out!
  randomSeed(analogRead(0));
  Serial.begin(115200); // Sometimes 9600

  Serial.println("Huzzah32 + PropMaker Wing magic wand starting up!");
  Serial.println();

  // Turn on power to the PropMaker FeatherWing (LED JST strip connection and speaker)
  pinMode(33, OUTPUT);
  digitalWrite(33, HIGH);
  Serial.print ("PropMaker powered up!");

///// Audio Setup //////
  ledcSetup(0,1E5,12);
  ledcAttachPin(26,0); // Send audio out via pin 26. This is un-changable.
///// Audio Setup End /////

// Set up wireless comms START
// Set up wireless comms END

///////////////////////// Button Setup
/*  pinMode(BUTTON, INPUT); */

///////////////////////// Accelerometer Setup
  if (! lis.begin(0x18)) {   // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1);
  }
  Serial.println("LIS3DH found!");
  
  lis.setRange(LIS3DH_RANGE_8_G);   // 2, 4, 8 or 16 G!
  
  Serial.print("Range = "); Serial.print(2 << lis.getRange());  
  Serial.println("G");

// Adjust this number for the sensitivity of the 'click' force
// this strongly depend on the range! for 16G, try 5-10
// for 8G, try 10-20. for 4G try 20-40. for 2G try 40-80
#define CLICKTHRESHHOLD 80

    // 0 = turn off click detection & interrupt
    // 1 = single click only interrupt output
    // 2 = double click only interrupt output, detect single click
    lis.setClick(1, CLICKTHRESHHOLD);
///////////////////////// End Accelerometer Setup

// LED Setup
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  //FastLED.addLeds<LED_TYPE,DATA_PIN,CLK_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);

  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);

  int core = xPortGetCoreID();
  Serial.print("Main code running on core ");
  Serial.println(core);

  // -- Create the FastLED show task
  xTaskCreatePinnedToCore(FastLEDshowTask, "FastLEDshowTask", 2048, NULL, 2, &FastLEDshowTaskHandle, FASTLED_SHOW_CORE);
}
// END SETUP

// FastLED pattern variables START
// List of patterns to cycle through.  Each is defined as a separate function below.
//typedef void (*SimplePatternList[])();
//SimplePatternList gPatterns = { rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm };
//uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))
// FastLED pattern variables END

/////////////////////////////////////////////////////////////////////////
////////////////////////// Main Wand Loop ///////////////////////////////
/////////////////////////////////////////////////////////////////////////

void loop()
{
  clickCheck(); // Check fora click. Update click counter, if needed. (Used to be a button checker.)

  // Change what the wand is doing based on the number of clicks.
  // Dream (quite) Mode
  if (pressCount==1) {
    ledZero();
    addGlitter (20); // What's wrong with a little sparkle power? NUTTIN'!
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
  }
  // Dazzle Mode
  if (pressCount==2) {
    dazzleMode ();
  }
  // Doodle Mode
  if (pressCount ==3) {
    doodleMode ();
  }
  // Doodle Mode 2
  if (pressCount ==4) {
    doodleMode2 ();
  }
  // Daylight (Flashlight) Mode
  if (pressCount ==5) {
    fill_solid(leds, NUM_LEDS, 0xFFFFFF); // Fill White.
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
  }
  // Gremlin AttackMode
  if (pressCount ==6) {
    gremlinGame ();
  }
  // Spell Casting Mode
  if (pressCount ==7) {
    duel ();
  }
}

////////////////////////////////////////////////////////////////////////
////////////////////////// End Wand Loop ///////////////////////////////
////////////////////////////////////////////////////////////////////////

////// Dazzle Mode
void dazzleMode () {
  juggle ();
  FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
  }
////// END Dazzle Mode

////// Doodle Mode // Tilt left and right to change the color flowing from the tip.
void doodleMode () {
  CRGB doodleBallColor;
  smoothData();
  translatePosition();
  if (currentPosition != recentPosition) { 
    // Did the position change? If so...
    recentPosition = currentPosition; // Update the old position to match the current position.
    // select a color based on the direction varaible and fill the handle with it
    Serial.print (currentPosition);Serial.print(" | ");
    if (currentPosition == "d") {
      doodleBallColor = (CRGB::Green);
    } else if (currentPosition == "D") {
      doodleBallColor = (CRGB::IndianRed);
    } else if (currentPosition == "A") {
      doodleBallColor = (CRGB::White);
    } else if (currentPosition == "U") {
      doodleBallColor = (CRGB::Cyan);
    } else if (currentPosition == "u") {
      doodleBallColor = (CRGB::Yellow);
    } else if (currentPosition == "B") {
      doodleBallColor = (CRGB::Magenta);
    } else if (currentPosition == "L") {
      doodleBallColor = (CRGB::Red);
    } else if (currentPosition == "R") {
      doodleBallColor = (CRGB::Blue);
    }
  }
  // Paint the wand!
  Serial.println (currentPosition);
  for (int ledCount = 0; ledCount <14; ledCount++) {
    leds[ledCount] = leds[ledCount+1]; // Copy the left side of the wand (from 1 to 14) to leds[0 to 13]
    leds[29-(ledCount)] = leds[29-(ledCount+1)]; // Copy the right side of the wand (from 15 to 28) to leds[16 to 29]
  }
  //write the current color to the tip (14 and 15)
  leds[14]=doodleBallColor;
  leds[15]=doodleBallColor;
  FastLEDshowESP32();
  FastLED.delay(1000/FRAMES_PER_SECOND); // insert a delay to keep the framerate modest
}
////// END Doodle Mode

////// Doodle Mode2 - energyball!
void doodleMode2 () {
  CRGB doodleBallColor;
  smoothData();
  translatePosition();
  if (currentPosition != recentPosition) { 
    // Did the position change? If so...
    recentPosition = currentPosition; // Update the old position to match the current position.
    // select a color based on the direction varaible and fill the handle with it
    if (currentPosition == "d") {
      doodleBallColor = (CRGB::Green);
    } else if (currentPosition == "D") {
      doodleBallColor = (CRGB::IndianRed);
    } else if (currentPosition == "A") {
      doodleBallColor = (CRGB::White);
    } else if (currentPosition == "U") {
      doodleBallColor = (CRGB::Cyan);
    } else if (currentPosition == "u") {
      doodleBallColor = (CRGB::Yellow);
    } else if (currentPosition == "B") {
      doodleBallColor = (CRGB::Magenta);
    } else if (currentPosition == "L") {
      doodleBallColor = (CRGB::Red);
    } else if (currentPosition == "R") {
      doodleBallColor = (CRGB::Blue);
    } 
    // We have our color, let's make a shooting star!
    for (int ballStutter = 0; ballStutter < 2; ballStutter++) {
      for (int ledCount = 1; ledCount <15; ledCount++) {
        fill_gradient_RGB(leds, ledCount, doodleBallColor , ledCount-1, CRGB::Black);// Draw the ball on the left
        fill_gradient_RGB(leds, 29-ledCount, doodleBallColor , 30-ledCount, CRGB::Black); // Draw the ball on the right side
        FastLEDshowESP32();
        FastLED.delay(1000/FRAMES_PER_SECOND); // insert a delay to keep the framerate modest
      }
    }
  }
}
////// END Doodle Mode2

//////// Doodle Mode 3 ////////
void doodleMode3 () {
  //This routine "paints" the 8 pairs of LEDs all along the wand.
  int leftLedTip = (NUM_LEDS/2)-1; // 14 if NUM_LEDS is 30. Sets the tip for a range of 14 to 0 
  int rightLedTip = (NUM_LEDS/2); // 15 if NUM_LEDS is 30. Sets the tip for a range of 15 to 29
  smoothData();
  translatePosition();
  if (currentPosition != recentPosition) { 
    // Did the position change? If so...
    recentPosition = currentPosition; // Update the old position to match the current position.
    // Move all the LEDs down three steps from the tip.
    for (int shiftTheBits = 0; shiftTheBits < 12; shiftTheBits++) {
      leds[shiftTheBits] = leds[shiftTheBits+3]; // left side
      leds[29-shiftTheBits] = leds[26-shiftTheBits]; // right side
    }
//    for (int shiftTheBits = 0; shiftTheBits < 15; shiftTheBits++) {
//      leds[shiftTheBits] = leds[shiftTheBits+1]; // left side
//      leds[29-shiftTheBits] = leds[28-shiftTheBits]; // right side
//      FastLEDshowESP32();      
    }
    // Paint the tip
  if (currentPosition == "d") {
    // NOTE: All of the colors used below (and more) are part of hte FastLED library.
     // For all colors with samples, see: https://github.com/FastLED/FastLED/wiki/Pixel-reference
    leds[leftLedTip] = CRGB::Green; leds[rightLedTip] = CRGB::Green;
  } else if (currentPosition == "D") {
    leds[leftLedTip] = CRGB::IndianRed; leds[rightLedTip] = CRGB::IndianRed;
  } else if (currentPosition == "A") {
    leds[leftLedTip] = CRGB::White; leds[rightLedTip] = CRGB::White;
  } else if (currentPosition == "U") {
    leds[leftLedTip] = CRGB::Cyan; leds[rightLedTip] = CRGB::Cyan;
  } else if (currentPosition == "u") {
    leds[leftLedTip] = CRGB::Yellow; leds[rightLedTip] = CRGB::Yellow;
  } else if (currentPosition == "B") {
    leds[leftLedTip] = CRGB::Magenta; leds[rightLedTip] = CRGB::Magenta;
  } else if (currentPosition == "L") {
    leds[leftLedTip] = CRGB::Red; leds[rightLedTip] = CRGB::Red;
  } else if (currentPosition == "R") {
    leds[leftLedTip] = CRGB::Blue; leds[rightLedTip] = CRGB::Blue;
  }
  // Blend from almost the tip to three LEDs in..
  fill_gradient_RGB(leds, 14, leds[14], 11, leds[11]);
  fill_gradient_RGB(leds, 15, leds[15], 18, leds[11]);
  FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
}
//////// END Doodle Mode 3 ////////

///////////////////////////////////////////////////////////////////////
///////////////////////Main Gremlin Game Loop//////////////////////////
///////////////////////////////////////////////////////////////////////
void gremlinGame() { 
  getGremlinWandOrientation();
  updateAliens();
}
//////////////////////////////////////////////////////////////////////
///////////////////////END Gremlin Game Loop//////////////////////////
//////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///////////////////////Gremlin Game Procedures//////////////////////////
////////////////////////////////////////////////////////////////////////

void getGremlinWandOrientation() {
  smoothData ();
  translatePosition();
  if (currentPosition != recentPosition) // Did the position change? If so...
  {
    recentPosition = currentPosition; // Update the old position to match the current position.
    positionQueue += currentPosition; // Add the new position to the Position Queue.
    positionQueue.remove(0,1);  // Serial.println("New Position Queue:");  Serial.println(positionQueue); // Remove the oldest character from the left of the Position Queue string.
    setEnergyBallColor();
    checkForThrow();
  }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Gun Update Stuff //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void setEnergyBallColor() {
  // select a color based on the direction varaible and fill the handle with it
//  if (currentPosition == "d") {
//    energyBallColor = (CRGB::Green);
//  } else if (currentPosition == "D") {
//    energyBallColor = (CRGB::IndianRed);
//  } else if (currentPosition == "A") {
//    energyBallColor = (CRGB::White);
//  } else if (currentPosition == "U") {
//    energyBallColor = (CRGB::Cyan);
//  } else if (currentPosition == "u") {
//    energyBallColor = (CRGB::Yellow);
//  } else if (currentPosition == "B") {
//    energyBallColor = (CRGB::Magenta);
/*  } else*/ if (currentPosition == "L") {
    energyBallColor = (CRGB::Red);
  } else if (currentPosition == "R") {
    energyBallColor = (CRGB::Blue);
  }
  displayEnergyBall();
}

void displayEnergyBall() {
  // We have our color, let's display it!
  leds[0] = energyBallColor;
  leds[NUM_LEDS-1] = energyBallColor;
  FastLED.show();
}

void checkForThrow() {
  // If the wand is pointing up or down, throw the ball.
  if ((currentPosition == "d") || (currentPosition == "D") || (currentPosition == "U") || (currentPosition == "u")) {
    throwTheBall();
  }
}

void throwTheBall () {
  int collision = 0;
  int energyBallPosition = 0;
  while (collision==0) {
    nextEnergyBallPosition = leds[energyBallPosition+1]; // Peek ahead one step in the strip.
    if (!nextEnergyBallPosition) { // If the next space is vacant... Note - ( leds[i] ) is false if vacant and true if it has any non-Black value.
      leds[energyBallPosition] = CRGB::Black; // Black out the current energy ball postion,
      leds[29-energyBallPosition] = CRGB::Black; // Black out the current energy ball postion,
      energyBallPosition ++; // move a step ahead
      leds[energyBallPosition] = energyBallColor; // Draw the e-ball in its new location
      leds[29-energyBallPosition] = energyBallColor; // Draw the e-ball in its new location
      FastLED.show(); // and update the display
      delay(20); // Slow the ball down just a little...
    } else {
      collision = 1; // the energy ball has hit an alien!
      alienHit();
    }
  }
}

void alienHit() {
  Serial.println("Alien hit!");
  CRGB alienColor = leds[15+numberOfAliens-1]; // look at the first alien in line
  int hitAlienRed = leds[15+numberOfAliens-1].red; // what is the red value of the first alien in line?
  int energyBallRed = energyBallColor.red; // what is the red value of the energy ball?
  int hitAlienBlue = leds[15+numberOfAliens-1].blue; // what is the blue value of the first alien in line?
  int energyBallBlue = energyBallColor.blue; // what is the blue value of the energy ball?
  if (energyBallColor == alienColor) { // If the alien and the energy ball match colors
    Serial.println("Alien destroyed!");
    leds[15+numberOfAliens-1] = CRGB::Black; // erase the alien on the left
    leds[15+numberOfAliens] = CRGB::Black; // erase the bullet on the left
    leds[14-numberOfAliens+1] = CRGB::Black; // erase the alien on the right
    leds[14-numberOfAliens] = CRGB::Black; // erase the bullet on the right
    numberOfAliens --; // reduce the # of aliens - do this after you erase them or your count is off!
    aliensBanished ++; // increase the number of aliens banished.
    FastLED.show();
    if (aliensBanished > 99) {superWinLedFx();loseTheGame();}
    if (numberOfAliens < 1){winTheGame();}
  } else {
    Serial.println("Wrong color!!");
    leds[15+numberOfAliens] = CRGB::Black; // erase the bullet on the left
    leds[14-numberOfAliens] = CRGB::Black; // erase the bullet on the right
    FastLED.show();
    // add one to the mistake counter
  }
}

void winTheGame() {
  Serial.println("You won!");
  numberOfWins ++;
  for (int j=0; j<numberOfWins; j++) {
    for (int i=0; i<256; i++) {
      fill_rainbow( leds, NUM_LEDS, i, 10);
      FastLED.show(); // update the strip with all changes
    }
  }
  FastLED.clear (); // Clear the strip
  FastLED.show(); // update the strip with all changes
  if (aliensBanished > 99) {
    superWinLedFx();
  }
  restartTheGame();
}

void restartTheGame() {
  Serial.println("Resetting game.");
  if (numberOfAliens > 13) {numberOfTurnsElapsed=0;} // Player lost. Reset the turn and win count.
  numberOfAliens = 0;
  for (int headstart=0; headstart<2+numberOfWins; headstart++) { // Set up the board with three aliens.
    addAnAlien(); delay(100);
  }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Alien Update Stuff //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

void updateAliens() {
  int timeDecay = numberOfTurnsElapsed*10;
  if (timeDecay > 900) {timeDecay=900;}
  attackTimer ++ ;
  if (attackTimer > 1000-(timeDecay)) { // The game speeds with each passing turn
    attackTimer=0; // Reset the timer
    numberOfTurnsElapsed ++; // Add one to the number of turns elapsed.
    addAnAlien();
  }
}

void addAnAlien() {
  numberOfAliens ++; // Increment the number of aliens, of course!
  if (numberOfAliens > 14) {loseTheGame();} // If there are too many aliens, the base has been overwhelmed. End the game.
  selectARandomAlien();
  displayTheAlien();
}

void selectARandomAlien() {
  int randomAlien = random(2);
//  Serial.print (randomAlien);
  if (randomAlien < 1) {
    alienAttackerColor = (CRGB::Red);
  } else {
    alienAttackerColor = (CRGB::Blue);
  }
}

void displayTheAlien() {
  // Move all the aliens up one step.
  for (int alienCongaLine=numberOfAliens; alienCongaLine > 0; alienCongaLine--) {
    leds[15+alienCongaLine] = leds[15+alienCongaLine-1]; // Update the left side.
    leds[14-alienCongaLine] = leds[14-alienCongaLine+1]; // Update the right side.
  }
  leds[NUM_LEDS/2] = alienAttackerColor; // Display the alien on the left...
  leds[(NUM_LEDS/2)-1] = alienAttackerColor; // ...and on the right.
  FastLED.show();
}

void loseTheGame() {
  Serial.println("You lost!");
  displayScore();
  endGameFx();
  restartTheGame();
}

void endGameFx() {
  for (int i=0; i<256; i++) {
    fill_solid( leds, NUM_LEDS, CHSV(i,255,255-i));
    FastLED.show(); // update the strip with all changes
    delay(15);
  }
  FastLED.clear (); // Clear the strip
  FastLED.show(); // update the strip with all changes
}

void displayScore() {
  for (int roundScore=0; roundScore<numberOfWins; roundScore++) {
    // display nubmer of wins
  }
  int scorePosition=0;
  FastLED.clear (); // Clear the strip
  for (int alienScore=0; alienScore<aliensBanished; alienScore++) {
    leds[scorePosition]=CHSV(scorePosition*10, 255, 255);
    FastLED.show(); 
    scorePosition++; if (scorePosition>29) {scorePosition=0; fill_solid(leds, NUM_LEDS, CHSV(100,100,100));FastLED.show(); }
    delay(25);
  }
  numberOfWins = 0; aliensBanished = 0; // Game's over. Wipe the scores.
}

void superWinLedFx() {
  Serial.println("SUPER WIN!");
  for (int q=0; q<999; q++) {
    if (random(10)<1) {
      int randomHue=random(256);
      int randomLED=random(30);
      leds[randomLED] = CHSV ((randomHue), 200, 255);
      FastLED.show();
      delay(15);
      blur1d(leds, NUM_LEDS, 60);
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/////////////////////// END Gremlin Game Procedures //////////////////////////
//////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////
////////////////////////// Main Duel Loop ///////////////////////////////
/////////////////////////////////////////////////////////////////////////

void duel() {
  checkForManaGain(); // Is it time to gain mana?
  listenForASpell(); //Listen for an incoming hex
  getWandOrientation(); // Get Wand Orientation (and possibly cast a spell).
  listenForASpell(); //Listen for an incoming hex
  checkForHexTimeout(); // If you're hexed, have you failed to respond to your hex in time?
  checkForOpponentHexTimeout(); // Is your opponent un-hexed?
  displayStats();
}

////////////////////////////////////////////////////////////////////////
////////////////////////// End Duel Loop ///////////////////////////////
////////////////////////////////////////////////////////////////////////

////// Click Check routine

void clickCheck() {
  // Check for click.
  uint8_t click = lis.getClick();
  if (click == 0) return;
  if (! (click & 0x30)) return;
  Serial.print("Click detected (0x"); Serial.print(click, HEX); Serial.print("): ");
  if (click & 0x10) Serial.print(" single click");
  Serial.println();
  pressCount += 1; // Increase the counter by 1.
  if (pressCount > 7) {pressCount = 1;} // Don't exceed the nubmer of wand modes!
  Serial.print("Mode #: "); Serial.println(pressCount);
  TotalNumberOfClicks += 1;
  Serial.print("Total Number of Clicks: "); Serial.println(TotalNumberOfClicks);
  ledZero(); // Clear the leds before changing modes.
  delay(250);
  return;
}

////// END Click Check routine

////// Duel supporting procedures //////

//Check to see if it's time to gain mana.
void checkForManaGain() {
  if (millis() >= nextThreshold) {
    currentThreshold = millis();
    nextThreshold = currentThreshold + thresholdGapValue;
    if (currentMana >= maximumMana)
      {
        currentMana = maximumMana; // Can not exceed max.mana limit.
      } else {
        currentMana += manaIncrementValue; // If not full, gain mana.
        // Serial.print("."); // ...and print a dot.
      }
  }
}

// Listen for an incoming spell
void listenForASpell() {
  // This may be moot - We may set up a listening daemon.
}

// Get the real accel of the want and smooth / simplfy it.
void smoothData () {
  /* Create an event structure to read the accelerometer with */
  sensors_event_t event; 
  lis.getEvent(&event);
  
  /* Set "real" accel variables equal to readings, refactored to one gee */
  xreal = event.acceleration.x/9.8;
  yreal = event.acceleration.y/9.8;
  zreal = event.acceleration.z/9.8;

  /* "Round" real gee readings to nearest .5 (with a reasonable noise buffer and "stickyness")
  and stash the rounded gee in the corresponding "prime" variable. */
  if ((xreal >= -1) && (xreal <= -.85)) {
    xprime =-1;
  } else if ((xreal >= -.65) && (xreal <= -.35)) {
    xprime =-.5;
  } else if ((xreal >= -.15) && (xreal <= .15)) {
    xprime =0;
  } else if ((xreal >= .35) && (xreal <= .65)) {
    xprime =.5;
  } else if ((xreal >= .85) && (xreal <= 1)) {
    xprime =1;
  }
   
  if ((yreal >= -1) && (yreal <= -.85)) {
    yprime =-1;
  } else if ((yreal >= -.65) && (yreal <= -.35)) {
    yprime =-.5;
  } else if ((yreal >= -.15) && (yreal <= .15)) {
    yprime =0;
  } else if ((yreal >= .35) && (yreal <= .65)) {
    yprime =.5;
  } else if ((yreal >= .85) && (yreal <= 1)) {
    yprime =1;
  }
   
  if ((zreal >= -1) && (zreal <= -.85)) {
    zprime =-1;
  } else if ((zreal >= -.65) && (zreal <= -.35)) {
    zprime =-.5;
  } else if ((zreal >= -.15) && (zreal <= .15)) {
    zprime =0;
  } else if ((zreal >= .35) && (zreal <= .65)) {
    zprime =.5;
  } else if ((zreal >= .85) && (zreal <= 1)) {
    zprime =1;
  } 
}

// Turn smoothed accel data into a  simplified position character:
// U = Up, D = Down, L = Left, R = Right, A = straight Ahead, B = pointing back
// u = ahead and "u"p, at a 45º angle, d = ahead and  "d"own at a 45º angle
// These eight positions are all the wand really needs to know in order to cast a spell
// Having more positions than this makes it easier to mess a spell up.
void translatePosition () {
  primesum = xprime+yprime+zprime;

  if (primesum == 1 || primesum == -1) {
    if (xprime == 1) {
      currentPosition = "A"; // Ahead (Was Right)
    } else if (xprime == -1) {
      currentPosition = "B"; // Back (or inverted) (Was Left)
    } else if (yprime == 1) {
      currentPosition = "D"; // Down --- ok
    } else if (yprime == -1) {
      currentPosition = "U"; // Up --- ok
    } else if (zprime == 1) {
      currentPosition = "L"; // Left (Was Ahead)
    } else if (zprime == -1) {
      currentPosition = "R"; // Right (Was Back)
    } else if (yprime == .5) {
      if (xprime == .5) { //was zprime
        currentPosition = "d"; // Ahead down @ 45º up ... ok
      }
    } else if (yprime == -.5) {
      if (xprime == -.5) {
        currentPosition = "B"; // Back, technically at 45º, but we're clustering it into the "back" category ... ok
      }
    }
  }

  if (primesum == 0 ) {
    if (yprime == -.5) { 
        if (xprime == .5) { // Was zprime
          currentPosition = "u"; // Ahead, up @ 45º ... ok
        }
    }
  }
}// END translatePosition

//Record the wand's orientation
void getWandOrientation() {
  smoothData ();
  translatePosition();
  if (currentPosition != recentPosition) // Did the position change? If so...
  {
    recentPosition = currentPosition; // Update the old position to match the current position.
    playPositionTone(); 
    positionQueue += currentPosition; // Add the new position to the Position Queue.
    positionQueue.remove(0,1);  Serial.println("New Position Queue:");  Serial.println(positionQueue); // Remove the oldest character from the left of the Position Queue string.
    castASpell(); // Since the wand's moved, check if a spell was cast.
  }
}

void playPositionTone() {
  if (currentPosition=="A") {
    ledcWriteNote(0,NOTE_C,octave); delay(200);
    ledcWriteTone(0,0); delay(50);
  } else if (currentPosition=="d") {
    ledcWriteNote(0,NOTE_D,octave); delay(200);
    ledcWriteTone(0,0); delay(50);
  } else if (currentPosition=="u") {
    ledcWriteNote(0,NOTE_E,octave); delay(200);
    ledcWriteTone(0,0); delay(50);
  } else if (currentPosition=="L") {
    ledcWriteNote(0,NOTE_F,octave); delay(200);
    ledcWriteTone(0,0); delay(50);
  } else if (currentPosition=="U") {
    ledcWriteNote(0,NOTE_G,octave); delay(200);
    ledcWriteTone(0,0); delay(50);
  } else if (currentPosition=="D") {
    ledcWriteNote(0,NOTE_A,octave); delay(200);
    ledcWriteTone(0,0); delay(50);
  } else if (currentPosition=="R") {
    ledcWriteNote(0,NOTE_B,octave); delay(200);
    ledcWriteTone(0,0); delay(50);
  } else if (currentPosition=="B") {
    ledcWriteNote(0,NOTE_C,octave+1); delay(200);
    ledcWriteTone(0,0); delay(50);
  }
}

//Check to see if you cast a spell
void castASpell() {
//  if (!isOpponentHexed) { // If the opponent is not hexed, see if you can cast a spell...
/*    if (positionQueue.endsWith ("202")) // Start a duel - point straight up, then down, then up.
      {spellDuel();}
    else */if (positionQueue.endsWith ("ALAuUBUu")) // Fireball - Was: "DdAuUBUu" point down, then over  right shoulder, then straight ahead
      {spellFireball(); checkForAHex();}
    else if ((positionQueue.endsWith ("ALALBRA")) || (positionQueue.endsWith ("ALARBLA"))) // Firejet
      {spellFirejet(); checkForAHex();}
    else if (positionQueue.endsWith ("ARAuUBUu")) // Waterball - Was: "ARUuA" Starting straight, point right (clockwise) and back, then up,then point ahead ** ALT 62324
      {spellWaterball(); checkForAHex();}
    else if ((positionQueue.endsWith ("ARALBRA")) || (positionQueue.endsWith ("ARARBLA"))) // Waterjet
      {spellWaterjet(); checkForAHex();}
    else if (positionQueue.endsWith ("AuAuUBUu")) // Airball - Was: "ALARAL" Starting straight, twist wand CCW 90º, then CW unitl 90º, then back to 90º left.
      {spellAirball(); checkForAHex();}
    else if ((positionQueue.endsWith ("AuALBRA")) || (positionQueue.endsWith ("AuARBLA"))) // Airjet
      {spellAirjet(); checkForAHex();}
    else if (positionQueue.endsWith ("AdAuUBUu")) // Earthball - Was: "DdALA" point at the ground, then ahead, then left, then point straight ahead again.
      {spellEarthball(); checkForAHex();}
    else if ((positionQueue.endsWith ("AdALBRA")) || (positionQueue.endsWith ("AdARBLA"))) // Earthjet
      {spellEarthjet(); checkForAHex();}
    else if ((positionQueue.endsWith ("ARAuUL")) || (positionQueue.endsWith ("ARAuUR"))) // Wall of Water - Was: "uRBUu" Starting out and 45º up, point to the right, then up, then ahead.
      {spellWaterWall(); checkForAHex();}
    else if ((positionQueue.endsWith ("ALAuUL")) || (positionQueue.endsWith ("ALAuUR"))) // Wall of Fire - Was: "ARBUB" Starting straight, twist CW until inverted (180º), then bend elbow to point at sky, then extend (keeping top down). 
      {spellFireWall(); checkForAHex();}
    else if ((positionQueue.endsWith ("AuAuUL"))|| (positionQueue.endsWith ("AuAuUR"))) // Wall of Air - Was: "ALBRA" Circle point around counterclockwise, toward self.
      {spellAirWall(); checkForAHex();}
    else if ((positionQueue.endsWith ("AdAuUL")) || (positionQueue.endsWith ("AdAuUR"))) // Wall of Earth - Was: "AuLB" Rising left block (Point ahead, then lift wand while twisting palm out to point wand left.)
      {spellEarthWall(); checkForAHex();}
    else if ((positionQueue.endsWith ("ARUuA")) || (positionQueue.endsWith ("ALUuA"))) // Juggle demo - Starting straight, point right (clockwise) and back, then up,then point ahead ** ALT 62324
      {ledJuggle();}
    else if (positionQueue.endsWith ("ALARAL")) // Glitter Rainbow demo - Starting straight, twist wand CCW 90º, then CW unitl 90º, then back to 90º left.
      {ledGlitterbow();}
    else if (positionQueue.endsWith ("DdALA")) // Confetti demo – Point at the ground, then ahead, then left, then point straight ahead again.
      {ledConfetti();}
    else if ((positionQueue.endsWith ("ALBRA")) || (positionQueue.endsWith ("ARBLA"))) // BPM demo - Circle point around counterclockwise, toward self.
      {ledBpm();}
    else
      Serial.print("Fizzle");// {spellFizzle(); /* Serial.print("Post-cast Position Queue ="); Serial.println(positionQueue); */}
  } /*else {
    Serial.println("Opponent is already hexed!"); // Opponent is already hexed. Gotta wait!
    spellFizzle();
  }
}*/

void adjustMana() {
  currentMana-=150; // For now, all spells cost 150 mana.
  checkForOvercasting(); // Did you over-spend?
}

void checkForOvercasting() { // If you don't have enough mana, your wand will lose durability.
  if (currentMana <0) {
    Serial.print("You've overcast. Deficit: "); /// Let the player know they overcast.
    Serial.print(currentMana);
    Serial.println(" mana.");
    wandDurability += currentMana; // Reduce the wand's durability by the deficit...
    Serial.print("Current Durability = "); // Let the player know the wand's current durability.
    Serial.println(wandDurability);
    currentMana = 0; // ...and set current mana to zero.
    checkForCollapse(); // Has the wand lost all of its durability?
  } else {
    displayCurrentMana();
  }
}

void checkForCollapse() { // If your wand's durability is zero or less, its matrix has collapsed and you lose the duel.
  if (wandDurability <= 0) {
    Serial.println("Oh no!");
    endTheDuel ();
  }
}

void endTheDuel () {
  Serial.println("Your wand's matrix has collapsed!");
  // Let your opponent know they won.
  //char radiopacket[20] = "You win!"; // Set the message.
  // Serial.print("Sending "); Serial.print(radiopacket); Serial.println(" to opponent."); // Let us know we're sending.
  // rf69.send((uint8_t *)radiopacket, strlen(radiopacket)); // Send the message.
  // rf69.waitPacketSent(); // Wait until they receive it.
  //   ^^^^^^^^^^^^^^ NOTE - This should hang the wand if no-one is listening.
  wandDurability = 1000; // For this prototype, we're just resetting the game.
}

void checkForAHex() {
  if (amIHexed) { // If you're hexed and... 
    if (mostRecentSpell.startsWith("Wall")) { // ...you cast a defense in response to the Hex.
      calculateDefense(); // Figure out how effective the defense was.
      Serial.print("Hex defended against. Current Durability = ");
      Serial.println(wandDurability);
      amIHexed = false; // Drop the hex.
    } else { // ...you ignored the hex and cast a non-defense spell.
      wandDurability -= random(80,120); // Take 80 to 120 points of damage.
      Serial.print("Hex ignored and triggered. Ouch! Current Durability = ");
      Serial.println(wandDurability);
      amIHexed = false; // Drop the hex.
      checkForCollapse(); // Did you lose?
    }
  }
}

void calculateDefense() {
  char hexIndex = mostRecentHex.charAt(0);
  Serial.print("Current hex starts with ");
  Serial.println(hexIndex);
  if (
    (currentSpell.endsWith("Water") && hexIndex == 70) || // Fire
    (currentSpell.endsWith("Fire") && hexIndex == 87) || // Water
    (currentSpell.endsWith("Air") && hexIndex == 69) || // Earth
    (currentSpell.endsWith("Earth") && hexIndex == 65) // Air
  ) {
    Serial.println("Perfect Defense!"); // Caster defended with the exact opposed element.
  } else if (
    (currentSpell.endsWith("Water") && hexIndex == 87) || // Water
    (currentSpell.endsWith("Fire") && hexIndex == 70) || // Fire
    (currentSpell.endsWith("Air") && hexIndex == 65) || // Air
    (currentSpell.endsWith("Earth") && hexIndex == 69) // Earth
  ) {
    Serial.println("Poor defense..."); // Caster used the same element as the attack.
    wandDurability -= random(80,120); // Take 40 to 60 points of damage.
  } else {
    Serial.println("Imperfect defense."); // Caster used an element orthogonal to the attack.
    wandDurability -= random(40,80); // Take 40 to 60 points of damage.
  } 
}

void checkForHexTimeout() {
  if (amIHexed) {
    if (millis() >= whenIWasHexed+hexGapValue ) { // If you've run out of time...
      wandDurability -= random(80,120); // ...take 80 to 120 points of damage.
      Serial.print("Hex timed out and triggered. Ouch! Current Durability = ");
      Serial.println(wandDurability);
      amIHexed = false; // Drop the hex.
      checkForCollapse();
    }
  }
}

void checkForOpponentHexTimeout() {
  long timeSinceHexingOpponent = millis()-whenIHexedTheOpponent;
  if ( timeSinceHexingOpponent >= hexGapValue) { // Did you cast your last attack more than 15 seconds ago?
    isOpponentHexed = false; // If you did, drop the flag. They can't be hexed for more than 15 seconds.
  //  Serial.println("Opponent un-hexed.");
  }
}

void displayCurrentMana() {
  Serial.print("Current mana: ");
  Serial.println(currentMana);
}

void checkForRepeatedSpell() {
  if (currentSpell==mostRecentSpell) {
    repeatedSpellCount +=1;
    currentMana -= repeatedSpellCount*repeatedSpellPenalty;
    Serial.print("Repeated spell penalty triggered. You've cast the same spell ");
    Serial.print(repeatedSpellCount+1);
    Serial.print(" times in a row. Additional mana cost: ");
    Serial.println(repeatedSpellCount*repeatedSpellPenalty);
    Serial.print("Current mana: ");
    Serial.println(currentMana);
    checkForOvercasting();
  } else {
    repeatedSpellCount = 0;
  }
}

void displayStats() { // Show Mana and Durability and curent spell queue
  float manaPercentage = (currentMana*1.0)/maximumMana;
  int manaFillEnd = (4*manaPercentage)+1;
  ledZero();
  fill_gradient_RGB (leds,0,0x0000ff,manaFillEnd,0x000000); // Display current mana as a blue (0x0000ff) bar on the wand that goes from the third LED near the handle to whereever the "fill end" is. The fill end is black (0x000000).
  float durabilityPercentage = wandDurability/1000.0;
  int durabilityFillEnd = NUM_LEDS-2-(4*durabilityPercentage);
  fill_gradient_RGB (leds,NUM_LEDS-1,0xff0000, durabilityFillEnd,0x000000); // Display current mana as a red (oxff0000) bar on the wand that goes from the LED closest to the handle to whereever the "fill end" is. The fill end is black (0x000000).
  displayCurrentGestureString ();
  FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
}

void displayCurrentGestureString () {
  //This routine "paints" the 8 pairs of LEDs at the tip with the last 8 positions.
  //We're painting the whole thing instead of shifting LED vaules down and only painting the tip because spells perform full-wand animations, which would erase the LEDs we're referring to.
  String queueCharacter = "x";
  int leftLedTip = (NUM_LEDS/2)-1; // 14 if NUM_LEDS is 30. Sets the tip for a range of 14 to 0 
  int rightLedTip = (NUM_LEDS/2); // 15 if NUM_LEDS is 30. Sets the tip for a range of 15 to 29
  // start at the end (length) of the positionQueue (the 20th character) • end when you've read 8 characters • Reduce your read position by one per loop
  for (int charReadPointer=positionQueue.length(); charReadPointer > positionQueue.length()-9; charReadPointer--) {
    queueCharacter = positionQueue.charAt(charReadPointer); // What's the character at the ReadPointer?
    if (queueCharacter == "d") {
      // NOTE: All of the colors used below (and more) are part of hte FastLED library.
       // For all colors with samples, see: https://github.com/FastLED/FastLED/wiki/Pixel-reference
      leds[leftLedTip-(19-charReadPointer)] = CRGB::Green; leds[rightLedTip-(charReadPointer-19)] = CRGB::Green;
    } else if (queueCharacter == "D") {
      leds[leftLedTip-(19-charReadPointer)] = CRGB::IndianRed; leds[rightLedTip-(charReadPointer-19)] = CRGB::IndianRed;
    } else if (queueCharacter == "A") {
      leds[leftLedTip-(19-charReadPointer)] = CRGB::White; leds[rightLedTip-(charReadPointer-19)] = CRGB::White;
    } else if (queueCharacter == "U") {
      leds[leftLedTip-(19-charReadPointer)] = CRGB::Cyan; leds[rightLedTip-(charReadPointer-19)] = CRGB::Cyan;
    } else if (queueCharacter == "u") {
      leds[leftLedTip-(19-charReadPointer)] = CRGB::Yellow; leds[rightLedTip-(charReadPointer-19)] = CRGB::Yellow;
    } else if (queueCharacter == "B") {
      leds[leftLedTip-(19-charReadPointer)] = CRGB::Magenta; leds[rightLedTip-(charReadPointer-19)] = CRGB::Magenta;
    } else if (queueCharacter == "L") {
      leds[leftLedTip-(19-charReadPointer)] = CRGB::Red; leds[rightLedTip-(charReadPointer-19)] = CRGB::Red;
    } else if (queueCharacter == "R") {
      leds[leftLedTip-(19-charReadPointer)] = CRGB::Blue; leds[rightLedTip-(charReadPointer-19)] = CRGB::Blue;
    }
  }
//  FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip // Commented out because Show is called immediately after this in displayStats()
}

////////////////////////////// Misc LED FX

//////////////////////////////////////
/////// Fast LED FX procedures ///////
//////////////////////////////////////

void rainbow() 
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}

void addGlitter( fract8 chanceOfGlitter) 
{
  if( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void rainbowWithGlitter() 
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void confetti() 
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 13, 0, NUM_LEDS-1 );
  leds[pos] += CHSV( gHue, 255, 192);
}

void sineball(int ballHue)
{
  // a ballHue-colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 13, 0, NUM_LEDS-1 );
  leds[pos] += CHSV( ballHue, 255, 192);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 62;
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) { //9948
    leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
  }
}

void juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 8; i++) {
    leds[beatsin16( i+7, 0, NUM_LEDS-1 )] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

void ElementalJetWithPalette(int jetColor) {
// Array of temperature readings at each simulation cell
  static byte heat[NUM_LEDS];

  // Step 0. Set the palette up
     uint8_t hue = jetColor; // Set the base hue to the jetcolor
     CRGB darkcolor  = CHSV(hue,255,192); // pure hue, three-quarters brightness
     CRGB lightcolor = CHSV(hue,128,255); // half 'whitened', full brightness
     gPal = CRGBPalette16( CRGB::Black, darkcolor, lightcolor, CRGB::White);
  
  // Step 1.  Cool down every cell a little
    for( int i = 0; i < NUM_LEDS; i++) {
      heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
    }
  
    // Step 2.  Heat from each cell drifts 'up' and diffuses a little
    for( int k= NUM_LEDS - 1; k >= 2; k--) {
      heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
    }
    
    // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
    if( random8() < SPARKING ) {
      int y = random8(7);
      heat[y] = qadd8( heat[y], random8(160,255) );
    }

    // Step 4.  Map from heat cells to LED colors
    for( int j = 0; j < NUM_LEDS/2; j++) {
      // Scale the heat value from 0-255 down to 0-240
      // for best results with color palettes.
      byte colorindex = scale8( heat[j], 240);
      CRGB color = ColorFromPalette( gPal, colorindex);
      int pixelnumber;
      pixelnumber = j;
      leds[pixelnumber] = color;
      leds[29-pixelnumber] = color;
    }
}


/////////////////////////////////////////////////
/////////// END Fast LED FX procedures //////////
/////////////////////////////////////////////////

////////////////////////////// SPELL BOOK

/////////////////////////////
/////// Attack Spells ///////
/////////////////////////////

void spellFireball() // Casts a fireball
// Fireball is a standard attack spell.
// Water defenses perfectly counter it.
{
  adjustMana();
  currentSpell = "Fireball";
  checkForRepeatedSpell();
  mostRecentSpell = "Fireball";
  Serial.println("Fireball!");
// ?Transmit spell?
  isOpponentHexed = true;
  whenIHexedTheOpponent = millis();
  ledFireball();
  ledZero();
}

void ledFireball() {
  int fx_timer = 0;
  do {
    sineball(0); // Call the sineball function with red (0), updating the 'leds' array
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
    fx_timer += 1;
  } while (fx_timer < 300);
  fx_timer = 0;
}

void spellEarthball() // Casts an earthball
// Earthball is a standard attack spell.
// Air defenses perfectly counter it.
{
  adjustMana();
  currentSpell = "Earthball";
  checkForRepeatedSpell();
  mostRecentSpell = "Earthball";
  Serial.println("Earthball!");
// ?Transmit spell?
  isOpponentHexed = true;
  whenIHexedTheOpponent = millis();
  ledEarthball();
  ledZero();
}

void ledEarthball() {
  int fx_timer = 0;
  do {
    sineball(96); // Call the sineball function with green (96), updating the 'leds' array
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
    fx_timer += 1;
  } while (fx_timer < 300);
  fx_timer = 0;
}


void spellAirball() // Casts an airball
// Airball is a standard attack spell.
// Earth defenses perfectly counter it.
{
  adjustMana();
  currentSpell = "Airball";
  checkForRepeatedSpell();
  mostRecentSpell = "Airball";
  Serial.println("Airball!");
// ?Transmit spell?
  isOpponentHexed = true;
  whenIHexedTheOpponent = millis();
  ledAirball();
  ledZero();
}

void ledAirball() {
  int fx_timer = 0;
  do {
    sineball(60); // Call the sineball function with yellow (64), updating the 'leds' array
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
    fx_timer += 1;
  } while (fx_timer < 300);
  fx_timer = 0;
}

void spellWaterball() // Casts a waterball
// Waterball is a standard attack spell.
// Fire defenses perfectly counter it.
{
  adjustMana();
  currentSpell = "Waterball";
  checkForRepeatedSpell();
  mostRecentSpell = "Waterball";
  Serial.println("Waterball!");
// ?Transmit spell?
  isOpponentHexed = true;
  whenIHexedTheOpponent = millis();
  ledWaterball();
  ledZero();
}

void ledWaterball() {
  int fx_timer = 0;
  do {
    sineball(144); // Call the sineball function with blue (160), updating the 'leds' array
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
    fx_timer += 1;
  } while (fx_timer < 300);
  fx_timer = 0;

}

void spellFirejet() // Casts a jet of fire
// Firejet is a quick attack spell - it hits instantly
// Water defenses perfectly counter it.
{
  adjustMana();
  currentSpell = "Firejet";
  checkForRepeatedSpell();
  mostRecentSpell = "Firejet";
  Serial.println("Firejet!");
// ?Transmit spell?
  isOpponentHexed = false;
  ledFirejet();
  ledZero();
}

void ledFirejet() {
  int fx_timer = 0;
  do {
    ElementalJetWithPalette(0); // Call the Jet function with the red palette set
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
    fx_timer += 1;
  } while (fx_timer < 300);
  fx_timer = 0;
}

void spellWaterjet() // Casts a jet of water
// Waterjet is a quick attack spell - it hits instantly
// Fire defenses perfectly counter it.
{
  adjustMana();
  currentSpell = "Waterjet";
  checkForRepeatedSpell();
  mostRecentSpell = "Waterjet";
  Serial.println("Waterjet!");
// ?Transmit spell?
  isOpponentHexed = false;
  ledWaterjet();
  ledZero();
}

void ledWaterjet() {
  int fx_timer = 0;
  do {
    ElementalJetWithPalette(144); // Call the Jet function with the blue palette set
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
     fx_timer += 1;
  } while (fx_timer < 300);
  fx_timer = 0;
}

void spellEarthjet() // Casts a jet of earth
// Earthjet is a quick attack spell - it hits instantly
// Air defenses perfectly counter it.
{
  adjustMana();
  currentSpell = "Earthjet";
  checkForRepeatedSpell();
  mostRecentSpell = "Earthjet";
  Serial.println("Earthjet!");
// ?Transmit spell?
  isOpponentHexed = false;
  ledEarthjet();
  ledZero();
}

void ledEarthjet() {
  int fx_timer = 0;
  do {
    ElementalJetWithPalette(96); // Call the Jet function with the green palette set
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
    fx_timer += 1;
  } while (fx_timer < 300);
  fx_timer = 0;
}

void spellAirjet() // Casts a jet of air
// Airjet is a quick attack spell - it hits instantly
// Earth defenses perfectly counter it.
{
  adjustMana();
  currentSpell = "Airjet";
  checkForRepeatedSpell();
  mostRecentSpell = "Airjet";
  Serial.println("Airjet!");
// ?Transmit spell?
  isOpponentHexed = false;
  ledAirjet();
  ledZero();
}

void ledAirjet() {
  int fx_timer = 0;
  do {
    ElementalJetWithPalette(60); // Call the Jet function with the green palette set
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
    fx_timer += 1;
  } while (fx_timer < 300);
  fx_timer = 0;
}

//////////////////////////////
/////// Defense Spells ///////
//////////////////////////////

void spellFireWall() // Creates a wall of fire
// Wall of Fire is a standard defense spell.
// It perfectly counters water attacks.
{
  adjustMana(); 
  currentSpell = "Wall of Fire";
  checkForRepeatedSpell();
  mostRecentSpell = "Wall of Fire";
  Serial.println("Wall of Fire!");
  ledFirewall();
  ledZero();
}

void ledFirewall() {
  fill_solid(leds, NUM_LEDS, CRGB::Red);
  FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
  delay(1500);
  ledZero();
}

void spellAirWall() // Creates a wall of air
// Wall of Air is a standard defense spell.
// It perfectly counters Earth attacks.
{
  adjustMana(); 
  currentSpell = "Wall of Air";
  checkForRepeatedSpell();
  mostRecentSpell = "Wall of Air";
  Serial.println("Wall of Air!!");
  ledAirwall();
  ledZero();
}

void ledAirwall() {
  fill_solid(leds, NUM_LEDS, CRGB::Yellow);
  FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
  delay(1500);
  ledZero();
}



void spellEarthWall() // Creates a wall of earth
// Wall of Earth is a standard defense spell.
// It perfectly counters Air attacks.
{
  adjustMana(); 
  currentSpell = "Wall of Earth";
  checkForRepeatedSpell();
  mostRecentSpell = "Wall of Earth";
  Serial.println("Wall of Earth!");
  ledEarthwall();
  ledZero();
}

void ledEarthwall() {
  fill_solid(leds, NUM_LEDS, CRGB::Green);
  FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
  delay(1500);
  ledZero();
}

void spellWaterWall() // Creates a wall of water
{
  adjustMana(); 
  currentSpell = "Wall of Water";
  checkForRepeatedSpell();
  mostRecentSpell = "Wall of Water";
  Serial.println("Wall of Water!");
  ledWaterwall();
  ledZero();
}

void ledWaterwall() {
  fill_solid(leds, NUM_LEDS, CRGB::Blue);
  FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
  delay(1500);
  ledZero();
}

///////////////////////////
/////// Misc Spells ///////
///////////////////////////

/*
void spellDuel() // Starts a duel
{
  // Dueling does not cost mana.
  mostRecentSpell = "Duel";
  Serial.println("Duel!");
  ledDuel();
  ledZero();
}

void ledDuel()
{
  for (int x=0; x <= 2; x++)
  {
    for (int i=255; i >= 105; i--)
    {
      // analogWrite(RED, i);
      analogWrite(GREEN, i);
      analogWrite(BLUE, i);
      delay(5);
      ledZero();
    }
  }
}
*/

void fastleddemoshow() {
    FastLEDshowESP32(); // send the 'leds' array out to the actual LED strip
    FastLED.delay(1000/FRAMES_PER_SECOND);   // insert a delay to keep the framerate modest
    // do some periodic updates
    EVERY_N_MILLISECONDS( 20 ) { gHue++; } // slowly cycle the "base color" through the rainbow
}

void ledJuggle() {
  int fx_timer = 0;
  do {
    juggle(); // Call the juggle function, updating the 'leds' array
    fastleddemoshow();
    fx_timer += 1;
  } while (fx_timer < 500);
  fx_timer = 0;
}

void ledBpm() {
  int fx_timer = 0;
  do {
    bpm(); // Call the bpm function, updating the 'leds' array
    fastleddemoshow();
    fx_timer += 1;
  } while (fx_timer < 500);
  fx_timer = 0;
}

void ledGlitterbow() {
  int fx_timer = 0;
  do {
    rainbowWithGlitter(); // Call the glitterbow function, updating the 'leds' array
    fastleddemoshow();
    fx_timer += 1;
  } while (fx_timer < 500);
  fx_timer = 0;
}

void ledConfetti() {
  int fx_timer = 0;
  do {
    confetti(); // Call the confetti function, updating the 'leds' array
    fastleddemoshow();
    fx_timer += 1;
  } while (fx_timer < 500);
  fx_timer = 0;
}

void spellFizzle() // Lets player know wand changed position.
{
  // Fizzling does not cost mana.
  // mostRecentSpell = "Fizzle"; // Technically, fizzling is not a spell. Should the wand retain the most recent "real" spell?
  Serial.println("***fizzle!***");
  ledFizzle();
  ledZero();
}

void ledFizzle() {
  // select a color based on the direction varaible and fill the strip with it
  if (currentPosition == "d") {
    fill_solid(leds, NUM_LEDS, CRGB::Green);
  } else if (currentPosition == "D") {
    fill_solid(leds, NUM_LEDS, 0xDDA0DD);
  } else if (currentPosition == "A") {
    fill_solid(leds, NUM_LEDS, 0xFFFFFF); // White
  } else if (currentPosition == "U") {
    fill_solid(leds, NUM_LEDS, 0x00FFFF); // Cyan
  } else if (currentPosition == "u") {
    fill_solid(leds, NUM_LEDS, CRGB::Yellow);
  } else if (currentPosition == "B") {
    fill_solid(leds, NUM_LEDS, 0xFF00FF); // Magenta
  } else if (currentPosition == "L") {
    fill_solid(leds, NUM_LEDS, CRGB::Red);
  } else if (currentPosition == "R") {
    fill_solid(leds, NUM_LEDS, 0x0000FF); // Blue
  } 
  FastLEDshowESP32(); // display it
  int fx_timer = 0;
  while (fx_timer < 300) {fx_timer+=1;} //wait a moment
  fx_timer = 0; // reset the timer
  ledZero(); // ...and remove the colors
}

/// Clear LEDS 
void ledZero() {
  FastLED.clear ();
  FastLEDshowESP32(); // update the strip with all changes
}