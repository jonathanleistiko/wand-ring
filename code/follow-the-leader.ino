#include <FastLED.h>

// Accelerometer libraries and such...
#include <Wire.h>
#include <SPI.h> // Needed if using the PropMaker Wing accelerometer
#include <Adafruit_LIS3DH.h> // For the PropMaker accelerometer
#include <Adafruit_Sensor.h> // For the PropMaker accelerometer

// Used for software SPI
#define LIS3DH_CLK 13
#define LIS3DH_MISO 12
#define LIS3DH_MOSI 11
// Used for hardware & software SPI
#define LIS3DH_CS 10
// software SPI
Adafruit_LIS3DH lis = Adafruit_LIS3DH(); // Instantiate access to the Accelerometer

// LED Definitions
#define NUM_LEDS 30
#define DATA_PIN 14

// Define the array of leds
CRGB leds[NUM_LEDS];

// Variables for accelerometer data simplification
float xreal = 0;
float yreal = 0;
float zreal = 0;
float xprime = 0; // Smoothed and simplified
float yprime = 0; // Smoothed and simplified
float zprime = 0; // Smoothed and simplified
float primesum = xprime+yprime+zprime; // Sum of all three simplified accels

// The wand can be in one of 8 positions, associated with the numbers 0 to 7.
String positionQueue = "88888888888888888888"; // The wand remembers the last 20 actions.
// Note: Seeding the positionQueue string with more (or fewer) characters changes how many positions the wand remembers.
char currentPosition = '8'; // The wand knows what its current positon is.
char recentPosition = '8'; // The wand knows what its most recent prior position was.

// Audio
int octave = 5; // Set the octave for notes / audio

////////////////////////////////////////////////////////////
////////////////////// Game Variables //////////////////////
////////////////////////////////////////////////////////////

int turnsElapsed=0;
String secretCode="";
bool playerPositionChanged = false; 

////////////////////////////////////////////////////////////
/////////////////////END Game Variables/////////////////////
////////////////////////////////////////////////////////////

void setup() {
  Serial.begin(115200); // Enable printed output

  ///////////////////////// LED Setup
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  // Turn on power to the PropMaker FeatherWing LED JST strip connection
  pinMode(33, OUTPUT);
  digitalWrite(33, HIGH);
  Serial.print ("PropMaker powered up!");

  ///////////////////////// Accelerometer Setup
  if (! lis.begin(0x18)) {   // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1);
  }
  Serial.println("LIS3DH found!");
  lis.setRange(LIS3DH_RANGE_8_G);   // 2, 4, 8 or 16 G
  Serial.print("Range = "); Serial.print(2 << lis.getRange());  
  Serial.println("G");
  #define CLICKTHRESHHOLD 80 // Adjust this number for the sensitivity of the 'click' force
  lis.setClick(1, CLICKTHRESHHOLD); // 0 = turn off click detection & interrupt • 1 = single click only interrupt output • 2 = double click only interrupt output, detect single click
  ///////////////////////// End Accelerometer Setup

///// Audio Setup ////// NOTE: If you comment out the next two lines, the game will not make noise.
  ledcSetup(0,1E5,12);
  ledcAttachPin(26,0); // Send audio out via pin 26. This is un-changable.
  ///// Audio Setup End /////

  ///////////////////////// Game Setup
  randomSeed(analogRead(0));
  createTheCode();
  Serial.print("Secret Code ="); Serial.println(secretCode);
}

void createTheCode() {
  // Create the Secret Code
  int priorNumber = 9; // We don't want to have the same position twice in a row, so we're tracking the prior number.
  for (int i=0; i<100; i++) {
    int randomNumber = random(0, 4);
    if (randomNumber == priorNumber) {
      randomNumber+=1;
      if (randomNumber > 3) {
        randomNumber=0;
      }
    }
    priorNumber = randomNumber;
    switch (randomNumber) {
      case 0:
        secretCode+="u";
        break;
      case 1:
        secretCode+="d";
        break;
      case 2:
        secretCode+="L";
        break;
      case 3:
        secretCode+="R";
        break;
    }
  }
}


////////////////////////////////////////////////////////////
////////////////////// Main Game Loop //////////////////////
////////////////////////////////////////////////////////////

void loop() {
  turnsElapsed++; // Increment the number of turns by one.
  showTheCode();
  getPlayerAnswer();
  if (turnsElapsed > 100) {endTheGame();}
  completedSequenceLeds();
}

////////////////////////////////////////////////////////////
//////////////////// END Main Game Loop ////////////////////
////////////////////////////////////////////////////////////

void showTheCode() {
  Serial.print("Turn # "); Serial.print(turnsElapsed);Serial.print(" >>> "); 
  for (int codePosition=0; codePosition<turnsElapsed; codePosition++) {
    char currentCharacter = secretCode.charAt(codePosition);
    Serial.print(currentCharacter);
    switch (currentCharacter) {
      case 'u':
        ledcWriteNote(0,NOTE_E,octave); 
        pulseWand(42);
        ledcWriteTone(0,0);
        break;
      case 'd':
        ledcWriteNote(0,NOTE_D,octave);
        pulseWand(85);
        ledcWriteTone(0,0);
        break;
      case 'L':
        ledcWriteNote(0,NOTE_F,octave);
        pulseWand(0);
        ledcWriteTone(0,0);
        break;
      case 'R':
        ledcWriteNote(0,NOTE_B,octave);
        pulseWand(171);
        ledcWriteTone(0,0);
        break;
    }
  }
 Serial.println("");
}

void pulseWand(int hue) {
  leds[2].setHue(hue); leds[27].setHue(hue);
  leds[7].setHue(hue); leds[22].setHue(hue);
  leds[12].setHue(hue); leds[17].setHue(hue);
  for (int i=0; i<60; i++) {
    blur1d(leds, NUM_LEDS, i);
    FastLED.show();
    if (i >= 20) {ledcWriteTone(0,0);}
    int flashDelay = 30;
    if (turnsElapsed> 60) {
      flashDelay = 5;
    } else if (turnsElapsed > 40) {
      flashDelay = 8;
    } else if (turnsElapsed > 20) {
      flashDelay = 10;
    } else if (turnsElapsed > 10) {
      flashDelay = 15;
    } else if (turnsElapsed > 5) {
      flashDelay = 20;
    }
    delay(flashDelay);
  }
}

void getPlayerAnswer() {
  for (int codePosition=0; codePosition<turnsElapsed; codePosition++) {
    playerPositionChanged = false; // Start by assuming that the player's position has not changed.
    Serial.print("Player response # "); Serial.print(codePosition); Serial.print(" of "); Serial.println(turnsElapsed);
    char currentCharacter = secretCode.charAt(codePosition);
    Serial.print("Target character = "); Serial.println(currentCharacter);
    while (!playerPositionChanged) {// As long as the position does not change...
       getWandOrientation(); // ...check for a change in orientation
    }
    displayPlayerAnswer();
    if (currentPosition != 'A') {
      evaluatePlayerAnswer(currentCharacter);
    } else {
      Serial.print ("A•");
      codePosition--; // To counter the +1 that will happen at the top of this "for" loop.
    }
  }
}

// Get the real accel of the want and smooth / simplfy it.
void smoothData () {
  /* Create an event structure to read the accelerometer with */
  sensors_event_t event; 
  lis.getEvent(&event);
  
  /* Set "real" accel variables equal to readings, refactored to one gee */
  xreal = event.acceleration.x/9.8;
  yreal = event.acceleration.y/9.8;
  zreal = event.acceleration.z/9.8;

  /* "Round" real gee readings to nearest .5 (with a reasonable noise buffer and "stickyness")
  and stash the rounded gee in the corresponding "prime" variable. */
  if ((xreal >= -1) && (xreal <= -.85)) {
    xprime =-1;
  } else if ((xreal >= -.65) && (xreal <= -.35)) {
    xprime =-.5;
  } else if ((xreal >= -.15) && (xreal <= .15)) {
    xprime =0;
  } else if ((xreal >= .35) && (xreal <= .65)) {
    xprime =.5;
  } else if ((xreal >= .85) && (xreal <= 1)) {
    xprime =1;
  }
   
  if ((yreal >= -1) && (yreal <= -.85)) {
    yprime =-1;
  } else if ((yreal >= -.65) && (yreal <= -.35)) {
    yprime =-.5;
  } else if ((yreal >= -.15) && (yreal <= .15)) {
    yprime =0;
  } else if ((yreal >= .35) && (yreal <= .65)) {
    yprime =.5;
  } else if ((yreal >= .85) && (yreal <= 1)) {
    yprime =1;
  }
   
  if ((zreal >= -1) && (zreal <= -.85)) {
    zprime =-1;
  } else if ((zreal >= -.65) && (zreal <= -.35)) {
    zprime =-.5;
  } else if ((zreal >= -.15) && (zreal <= .15)) {
    zprime =0;
  } else if ((zreal >= .35) && (zreal <= .65)) {
    zprime =.5;
  } else if ((zreal >= .85) && (zreal <= 1)) {
    zprime =1;
  } 
}

////////// Translating The Position //////////
// Turn smoothed accel data into a  simplified position character:
// U = Up, D = Down, L = Left, R = Right, A = straight Ahead, B = pointing back
// u = ahead and "u"p, at a 45º angle, d = ahead and  "d"own at a 45º angle
// These eight positions are all the wand really needs to know in order to cast a spell
// Having more positions than this makes it easier to mess a spell up.
void translatePosition () {
  primesum = xprime+yprime+zprime;

  if (primesum == 1 || primesum == -1) {
    if (xprime == 1) {
      currentPosition = 'A'; // Ahead
    } else if (xprime == -1) {
//      currentPosition = 'B'; // Back (or inverted)
    } else if (yprime == 1) {
      currentPosition = 'd'; // Down // NOTE - This was "D" not "d" but this game needs more "down" sensitivity.
    } else if (yprime == -1) {
      currentPosition = 'u'; // Up // NOTE - This was "U" not "u" but this game needs more "up" sensitivity.
    } else if (zprime == 1) {
      currentPosition = 'L'; // Left
    } else if (zprime == -1) {
      currentPosition = 'R'; // Right
    } else if (yprime == .5) {
      if (xprime == .5) {
        currentPosition = 'd'; // Ahead down @ 45º up
      }
    } else if (yprime == -.5) {
      if (xprime == -.5) {
//        currentPosition = 'B'; // Back, technically at 45º, but we're clustering it into the "back" category ... ok
      }
    }
  }

  if (primesum == 0 ) {
    if (yprime == -.5) { 
        if (xprime == .5) {
          currentPosition = 'u'; // Ahead, up @ 45º
        }
    }
  }
}// END translatePosition

//Record the wand's orientation
void getWandOrientation() {
  smoothData ();
  translatePosition();
  if (currentPosition != recentPosition) // Did the position change? If so...
  {
    playerPositionChanged = true; // Let the other routines know the positon changed.
    recentPosition = currentPosition; // Update the old position to match the current position.
    playPositionTone(); // Make a little beep!
    positionQueue += currentPosition; // Add the new position to the Position Queue.
    positionQueue.remove(0,1);  // Serial.println("New Position Queue:");  Serial.println(positionQueue); // Remove the oldest character from the left of the Position Queue string.
  }
}

void playPositionTone() {
  int toneDuration = 100;
  if (currentPosition=='A') {
    ledcWriteNote(0,NOTE_C,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=='d') {
    ledcWriteNote(0,NOTE_D,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=='u') {
    ledcWriteNote(0,NOTE_E,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=='L') {
    ledcWriteNote(0,NOTE_F,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=='U') {
    ledcWriteNote(0,NOTE_G,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=='D') {
    ledcWriteNote(0,NOTE_A,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=='R') {
    ledcWriteNote(0,NOTE_B,octave); delay(toneDuration);
    ledcWriteTone(0,0);
  } else if (currentPosition=='B') {
    ledcWriteNote(0,NOTE_C,octave+1); delay(toneDuration);
    ledcWriteTone(0,0);
  }
}

void displayPlayerAnswer() {
  // select a color based on the direction varaible and fill the handle with it
  switch (currentPosition) {
    case 'u':
      pulseWandPlayer(42);
      break;
    case 'd':
      pulseWandPlayer(85);
      break;
    case 'L':
      pulseWandPlayer(0);
      break;
    case 'R':
      pulseWandPlayer(171);
      break;
    case 'A':
      fill_solid (leds, NUM_LEDS, CHSV(0,20,20));
      FastLED.show();
      delay(50);
      ledZero();
  }
}

void pulseWandPlayer(int hue) {
  leds[4].setHue(hue); leds[25].setHue(hue);
  leds[10].setHue(hue); leds[19].setHue(hue);
  for (int i=0; i<60; i++) {
    blur1d(leds, NUM_LEDS, i);
    FastLED.show();
    int flashDelay = 30;
    if (turnsElapsed> 60) {
      flashDelay = 5;
    } else if (turnsElapsed > 40) {
      flashDelay = 8;
    } else if (turnsElapsed > 20) {
      flashDelay = 10;
    } else if (turnsElapsed > 10) {
      flashDelay = 15;
    } else if (turnsElapsed > 5) {
      flashDelay = 15;
    }
    delay(flashDelay);
  }
}

void evaluatePlayerAnswer(char currentCharacter) {
  if (currentPosition != currentCharacter) { // Is the player's answer different from the current code character?
    endTheGame(); // If so, it's wrong. End the game.
  } // Otherwise, keep playing.
}

void endTheGame() {
  Serial.println ("Bzzzzt!");
  ledcWriteNote(0,NOTE_C,octave-3);
  endGameLeds();
  ledcWriteTone(0,0);
  resetTheGame();
}

void endGameLeds() {
  for (int i=255; i>0; i--) {
    fill_solid (leds, NUM_LEDS, CHSV(0,0,i));
    FastLED.show();
  }
}

void resetTheGame() {
  secretCode = "";
  turnsElapsed = -1;
  createTheCode();
}

void completedSequenceLeds () {
  for (int i = 0; i<30; i++) {
    leds[i] = CHSV(random(255), 200, 200);
    leds[29-i] = CHSV(random(255), 200, 200);
    FastLED.show();
    delay(20);
  }
  ledZero();
}

/// Clear LEDS 
void ledZero() {
  FastLED.clear ();
  FastLED.show(); // update the strip with all changes
}