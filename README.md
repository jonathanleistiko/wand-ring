# Wand-ring

Code for Wand-Ring wands: Toy magic wands using the ESP32 microcontroller, an accelerometer, and "NeoPixel" addressable LEDs.
Wands communicate with each other over an ad-hoc wifi network, web configured network or a bluetooth interface.
